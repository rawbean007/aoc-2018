BEGIN {tot2=0;tot3=0}
{
split($0,line,"")
# Reset cnt array
split("", cnt, ":")
for(c in line){ cnt[line[c]]++;}
for (occ in cnt){
 if (cnt[occ] == 2) w2++
 if (cnt[occ] == 3) w3++
}
if (w2>0) tot2++;w2=0;
if (w3>0) tot3++;w3=0;
}
END {
print "w2="tot2
print "w3="tot3
print "w2*w3="tot2*tot3
}
